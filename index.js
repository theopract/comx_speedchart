import { options } from './js/chartOptions.js';
import { cumulativeToInterval, IntervalByHour } from './js/dataProcessors.js'

window.onload = function () {
  const dropZone = document.getElementById("drop_zone");
  const menuContainer = document.getElementsByClassName("download-csv__container");

  document.addEventListener("mousemove", function (e) {
    const showMenuCoordinates = window.innerWidth - 140;
    if (e.pageX >= showMenuCoordinates) {
      if (menuContainer[0].classList.contains("hidden")) {
        menuContainer[0].classList.remove("hidden");
        menuContainer[0].classList.add("visible");
      }
    } else {
      if (menuContainer[0].classList.contains("visible")) {
        menuContainer[0].classList.remove("visible");
        menuContainer[0].classList.add("hidden");
      }
    }
  });

  dropZone.addEventListener("dragover", handleDropZoneDragOver, false);
  dropZone.addEventListener("drop", handleDropZoneFileSelect, false);
  dropZone.addEventListener("dragleave", handleDropZoneDragLeave);

  function handleDropZoneFileSelect(evt) {
    evt.stopPropagation();
    evt.preventDefault();
    dropZone.classList.remove('drag-over-effect');

    // files is a FileList of File objects. List some properties.
    const files = evt.dataTransfer.files; // FileList object.

    const output = [];

    /**
    * @param { [ {meterName, utc, quantity, data = map(DateTime, Value)}, ...] } f  file from Files API
    * function return promise which returns desired content of a file, for now it is array of objects:
      {
        meter: "ID1",
        utc: "180",
        quantity: 'Active energy delivered (Wh)',
        data: Map(TimeStamp: '2018-06-20 23:10:00', Value: 89574734)
      } 
    */
    function getData(f) {
      return new Promise((resolve, reject) => {
        const reader = new FileReader();
        let quantity;
        let divider = 1; // используется для приведения к kWh
        let s = Object.create(null);
        reader.onload = e => {
          //console.log(reader.result);

          const fileContentByRow = reader.result.split(/\n/);
          s.site = fileContentByRow[1].split(",")[0];
          s.meterName = fileContentByRow[1].split(",")[4];
          s.utc = fileContentByRow[7].split(",")[1];
          quantity = fileContentByRow[6].split(",")[3];
          // example of quantity = "Active energy delivered (Wh)"
          s.quantity = quantity.match(/(.*(?=\s\())|(.*)/g)[0]; // Оставляем то, что до скобок, если они есть или целиком
          s.units = quantity.match(/(?<=\().*(?=\))/g)[0]; // Оставляем то, что в скобках или ничего

          /*             console.log(`Quantity is "${quantity}"`);
          console.log(`Measurement is "${s.quantity}"`);
          console.log(`Units is "${s.units}"`); */

          if (s.units === "Wh") {
            s.units = "kWh";
            divider = 1000;
          }

          const arrayOfDataRows = fileContentByRow.slice(7);

          // parse lines like this:
          // 0,180,2018-06-20 23:10:00,89574734
          // into map(Date.parse('2018-06-20 23:10:00'), 89574734)
          function buildMapData(dataRows) {
            const dataMap = new Map();
            dataRows.forEach(function (row) {
              const item = row.split(",");
              if (item[2]) dataMap.set(Date.parse(item[2]), parseFloat(item[3] / divider));
            });
            return dataMap;
          }

          const processedData = buildMapData(arrayOfDataRows);

          s.data = processedData;
          resolve(s);
        };
        reader.readAsText(f);
      });
    }

    const promiseList = [];
    for (let i = 0, f; (f = files[i]); i++) {
      const promise = getData(f);
      promiseList.push(promise);

      output.push(
        "<li><strong>",
        escape(f.name),
        "</strong> (",
        f.type || "n/a",
        ") - ",
        f.size,
        " bytes, last modified: ",
        f.lastModifiedDate.toLocaleDateString(),
        "</li>"
      );
    }

    /**
     * @param { [ {meterName, utc, quantity, data = map(DateTime, Value)}, ...] } dataArray
     * function to union array elements by meterName
     */
    function unionDataByMeter(dataArray) {
      let unionedData = [];
      let index;
      dataArray.forEach(function (elem, i) {
        if (
          unionedData.some(function (e, j) {
            if (e.meterName === elem.meterName) {
              index = j;
              return true;
            }
          })
        ) {
          unionedData[index].data = new Map([...unionedData[index].data, ...dataArray[i].data]); // merging two maps of pairs DateTime - Value
        } else {
          unionedData.push(elem);
        }
      });
      return unionedData;
    }



    Promise.all(promiseList).then(series => {
      const data = [];

      let filteredSeries = unionDataByMeter(series);
      let intervalSeries = cumulativeToInterval(filteredSeries);
      console.log({intervalSeries})
      let intervalByHourSeries = IntervalByHour(intervalSeries);

      console.table({
        measurement: series[0].quantity,
        units: series[0].units,
      });

      console.groupCollapsed(
        "%c Обычный series: ",
        "background: green; color: white; font-weight: bold;"
      );
      console.log(series);
      console.groupEnd();

      console.group(
        "%c Объединённый series: ",
        "background: green; color: white; font-weight: bold;"
      );
      console.log(filteredSeries);
      console.groupEnd();

      console.group(
        "%c Интервальный series: ",
        "background: green; color: white; font-weight: bold;"
      );
      console.log(intervalSeries);
      console.groupEnd();

      console.group(
        "%c Почасовой series: ",
        "background: green; color: white; font-weight: bold;"
      );
      console.log(intervalByHourSeries);
      console.groupEnd();

      options.series = [];
      //console.log(options);
      intervalSeries.forEach(function (serie) {
        options.series.push({
          name: serie.meterName,
          data: [...serie.data.entries()],
        });
      });

      options.title.text = series[0].site; //we set title for chart by first CSV processed file
      options.yAxis.title.text = `${series[0].quantity} (${series[0].units})`; //we set quantity for chart by first CSV processed file

      Highcharts.chart("chart-container", options);
    });

    document.getElementById("list").innerHTML = "<ul>" + output.join("") + "</ul>";
  }

  function handleDropZoneDragOver(evt) {
    evt.stopPropagation();
    evt.preventDefault();
    evt.dataTransfer.dropEffect = "copy"; // Explicitly show this is a copy.
    dropZone.classList.add('drag-over-effect');
  }

  function handleDropZoneDragLeave(evt) {
    evt.stopPropagation();
    evt.preventDefault();
    dropZone.classList.remove('drag-over-effect');
  }

  
  Highcharts.setOptions(options);

  const coll = document.getElementsByClassName("collapsible");

  for (let i = 0; i < coll.length; i++) {
    coll[i].addEventListener("click", function () {
      this.classList.toggle("active");
      const content = this.nextElementSibling;
      if (content.style.display === "block") {
        content.style.display = "none";
      } else {
        content.style.display = "block";
      }
    });
  }
};
