export const options = {
  "title": {
      "text": "Com'X Data"
  },
  "subtitle": {
      "text": "Date is UTC ignorant"
  },
  "xAxis": {
      "type": "datetime"
  },
  "yAxis": {
      "title": {
          "text": "Consumption, kWh"
      }
  },
  "legend": {
      "layout": "vertical",
      "align": "right",
      "verticalAlign": "middle"
  },
  "plotOptions": {
      "series": {
          "label": {
              "connectorAllowed": false
          }
      }
  },
  "responsive": {
      "rules": [
          {
              "condition": {
                  "maxWidth": 500
              },
              "chartOptions": {
                  "legend": {
                      "layout": "horizontal",
                      "align": "center",
                      "verticalAlign": "bottom"
                  }
              }
          }
      ]
  }
}