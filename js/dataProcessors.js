/**
 * @param { [ {meterName, utc, quantity, data = map(DateTime, Value)}, ...] } dataArray
 * function to convert cumulative values in data to interval values
 */
function cumulativeToInterval(dataArray) {
  let intervalData = [];
  let previousValue;
  let intervalMap;
  dataArray.forEach(function (e, j) {
    intervalMap = new Map();
    previousValue = 0;
    const mapAsc = new Map([...e.data.entries()].sort());
    mapAsc.forEach(function (value, key) {
      if (previousValue) {
        intervalMap.set(key, parseFloat((value - previousValue).toFixed(6)));
        previousValue = value;
      }
      previousValue = value;
    });
    intervalData.push({
      meterName: dataArray[j].meterName,
      utc: dataArray[j].utc,
      quantity: dataArray[j].quantity,
      units: dataArray[j].units,
      data: intervalMap,
    });
  });
  return intervalData;
}

function IntervalByHour(dataArray) {
  let output = [];
  let TimeStampHour;
  dataArray.forEach(function (e, j) {
    const hourMap = new Map();
    let sumValue;

    e.data.forEach(function (value, key) {
      TimeStampHour = new Date();

      TimeStampHour.setTime(key);

      // consumption with timestamp like 14:00 should go to the hour 13
      if (TimeStampHour.getMinutes() === 0) TimeStampHour.setHours(TimeStampHour.getHours() - 1);
      
      TimeStampHour.setMinutes(0);
      // TimeStampHour = new Date(TimeStampHour.getTime()).toString(); // if we want key to be like "Thu Nov 05 2020 10:00:00 GMT+0300 (Moscow Standard Time)"
      TimeStampHour = TimeStampHour.getTime() // otherwise it will be something like "1604559600000"

      sumValue = hourMap.get(TimeStampHour) || 0;
      hourMap.set(TimeStampHour, sumValue + value);

    });

    hourMap.forEach((value, key) => hourMap.set(key, value.toFixed(3)));

    output.push({
      meterName: dataArray[j].meterName,
      utc: dataArray[j].utc,
      quantity: dataArray[j].quantity,
      units: dataArray[j].units,
      data: hourMap,
    });

  });
  return output;
}

export { cumulativeToInterval, IntervalByHour };